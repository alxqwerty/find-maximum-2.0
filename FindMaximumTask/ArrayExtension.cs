using System;

namespace FindMaximumTask
{
    public static class ArrayExtension
    {
        public static int FindMaximum(int[] array)
        {
            if (array is null)
            {
                throw new ArgumentNullException(nameof(array), "Array is null.");
            }

            if (array == Array.Empty<int>())
            {
                throw new ArgumentException("Array is empty.", nameof(array));
            }

            if (array.Length == 1)
            {
                return array[0];
            }

            int maximum = array[0];

            for (int i = 0; i < array.Length; i++)
            {
                if (maximum < array[i])
                {
                    maximum = array[i];
                }
            }

            return maximum;
        }
    }
}
